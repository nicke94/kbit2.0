<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link href="css/my.css" rel="stylesheet">
  </head>
  <body>
    <div class="wrapper">
      <div class = "l-background>">
        <img src="./img/web_bg_Left.png" class="img-fluid hidden-sm-down" alt="Responsive image">
      </div>
    <div class="container">

      <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand" href="#">KBit 2017</a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav navbar-nav">
          <li class="nav-item ">
            <a class="nav-link" href="./index.php">Hem </a>
          </li>
      <li class="nav-item active">
            <a class="nav-link" href="./tickets.php">Biljetter</a>
          </li>
      <li class="nav-item">
            <a class="nav-link" href="./tournaments.php">Turneringar</a>
          </li>
      <li class="nav-item">
            <a class="nav-link" href="./schedule.php">Schema</a>
          </li>
      <li class="nav-item">
            <a class="nav-link" href="./contact.php">Kontakta oss</a>
          </li>
        </ul>
        </div>
      </nav>

    <img src="./img/web_top.png" class="img-fluid" alt="Responsive image">

  <?php
	include 'func.php';
	include 'connector.php';
	echo "<div class=\"starter-template\">";

	$sql = "SELECT * from backend";
	$resultat = $mysqli->query($sql);
	$rad = $resultat->fetch_assoc();
	#print_r ($rad);
	if($rad['enabled']){
    if($_GET['med'] == "success") {?>
    </br><h3>Success!</h3>
    <p>Er anmälan har nu registrerats hos oss! Ett bekräftelsemail kommer nu att skickas till den angivna mailadressen med betalningsinstruktioner.
    </br>Happy gaming!</p>
      <form method="post" action="./tickets.php">
    		<input type="hidden" name="" value="">
            <button class="btn btn-primary" type="submit">Tillbaka</button>
        </form></br>

        <?php
     }
		elseif(isset($_POST['formcheck']) == "true"){

      if(filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL) && strlen($_POST['fnamn']) && strlen($_POST['enamn']) && strlen($_POST['nick']) && strlen($_POST['pernum']) == 10 && is_numeric($_POST['pernum']) && strlen($_POST['mail']) && isset($_POST['rules'])){
          ///correct input in form
        $fnamn = $mysqli->real_escape_string($_POST['fnamn']);
				$enamn = $mysqli->real_escape_string($_POST['enamn']);
				$nick = $mysqli->real_escape_string($_POST['nick']);
				$pernum = $mysqli->real_escape_string($_POST['pernum']);
				$mail = $mysqli->real_escape_string($_POST['mail']);
				$t_type = $mysqli->real_escape_string($_POST['t_type']);

        //print_r($_POST);

				$time = date("Y-m-d H:i:s");
				$sql = "INSERT into registered (fname, ename, nick, pernum, email, time, type) VALUES ('$fnamn', '$enamn', '$nick', '$pernum', '$mail', '$time', '$t_type')";
				//echo $sql;
				$resultat = $mysqli->query($sql);

        $sql1 = "SELECT id from registered WHERE pernum='$pernum' AND time='$time'";
        $resultat = $mysqli->query($sql1);
        $row = $resultat->fetch_assoc();

        $pay_id = $pernum ."-". $row[id];

        if ($t_type == "smash"){
            $cost = '50kr';
        }
        else{
            $cost = '150kr';
        }
        mail_reg($mail,$pay_id,$cost,$fnamn, $enamn,$nick,$pernum,$t_type);
				$msg = "Anmäld till kbit :)";
        header("Location:tickets.php?med=success");
        //unset($_POST);

				}
			else{
        ///wrong input in form
       reg_form_error($_POST['fnamn'],$_POST['enamn'],$_POST['nick'],$_POST['pernum'],$_POST['mail'],$_POST['rules'],$_POST['t_type']);
				}

		}
		else{
      ///no submitted form
      reg_form();
			}
	}
	else{
		echo '<div><h3 class="center padd"> Biljettbokningen har tyvärr stängt</h3>
		<p class="center padd">Det går inte längre att förköpa biljetter, men det går fortfarande att betala på plats i entrén till KBit oavsett om du förbokat biljetten eller ej. (200kr för datorplats / 50kr för super smash)</p></div>

    <div class ="hidden-sm-down filler">

    </div>
    <img src="./img/web_bottom.png" class="img-fluid" alt="Responsive image">
  	</div>

    </div>
    ';
	}
  ?>


		<div class="collapse rules" id="rules-div" >
      <h3 id="rules">Regler</h3>
			<ul >
				<li>Åldersgräns: 15 år. Personer under 15 år endast i ansvarig myndigs sällskap</li>
				<li>Alkohol får endast införskaffas och förtäras i puben</li>
				<li>All form av narkotika är strikt förbjudet</li>
				<li>Förbjudet att sova i datorlokalen</li>
				<li>Kbits arrangörer tar inget ansvar för personliga tillhörigheter</li>
				<li>Stöld, stöldförsök, skadegörelse & datorintrång kommer att polisanmälas</li>
				<li>Virusskanning ska göras innan inkoppling sker på Kbits nät</li>
				<li>Vid upptäckt av virus har Kbit rätten att koppla bort enheten från nätet</li>
				<li>Varje person ansvarar för egen städning</li>
				<li>Piratkopierade program och spel är ej tillåtna</li>
				<li>Kbits arrangörer har rätt att avisa personer som missköter sig från lokalen</li>
				<li>Personer som avvisas får ej entréavgiften åter</li>
				<li>Egen dryck får endast medtagas om det är oöppnat</li>
				<li>Egen mat får medtagas</li>
				<li>Högtalare är inte tillåtna, endast hörlurar/headset</li>
				<li>Ev. nya regler kan tillkomma innan eller under LANets gång</li>
			</ul>
		</div>


  <div class = "r-background>">
    <img src="./img/web_bg_Right.png" class="img-fluid hidden-sm-down" alt="Responsive image">
  </div>

</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
