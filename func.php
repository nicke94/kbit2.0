<?php
function chk(){
		session_start();
    		if($_SESSION['chk'] == "inlogg") {
         	 }
			else{
				//echo "inlodsfdsfagg";
				header('location:login.php?med=fel');
			}
	}
	function mail_reg($to,$pay_id,$cost,$fnamn, $enamn,$nick,$pernum,$t_type){
		$subject = 'Anmälan till KBit 2017';
		$from = "noreply@kbit.mdfnet.se";
		//$message = 'Din anmälning är nu registrerad hos oss. </br>Betalningen på '. $cost. ' ska ske till bankgironummer XXXX-XXXX med referens/OCR: '. $pay_id . ' innan 27/3-17';
		$headers = "Content-type: text/plain; charset=UTF-8";
		//$headers .= 'From: noreply@kbit.se';
		if ($t_type == "vanlig"){
				$ticket = "vanlig biljett (BYOC/datorplats)";
			}
		else{
				$ticket = "super smash turneringsbiljett";

		}
 $message = 'Hej '.$fnamn.'!

Din bokning för en "'. $ticket .'" till KBit 2017 är nu registrerad hos oss.

=== Biljettinformation ===
Biljettyp: '.$ticket.'
Biljettpris: '.$cost.'
Namn: '.$fnamn ." ". $enamn .'
Smeknamn: '.$nick.'
Personnummer: ' . $pernum .'

Betalningen på '.$cost.' ska ske till:
Mälardalens Datorförening
Bankgironummer: 5650-4251
Referens/OCR: '.$pay_id.'
Sista betalningsdag: 29/3-17

Om något av informationen ovan är felaktigt så är det bara att göra en helt ny bokning på kbit.se/tickets.php
Om du har frågor om något eller stöter på problem, kontakta oss på Facebook via meddelande: https://www.facebook.com/kbitLAN/

MVH
KBit Crew';
		$ok = @mail($to, $subject, $message, $headers, "-f " . $from);
	}
	function reg_form(){
			?>
			<h3 class = "center padd"> Anmälan till KBit 2017</h3><br>
      <div class ="center">
      <p center>
				<b>OBS!! SISTA CHANSEN ATT BETALA BILJETTEN ONLINE ÄR 29e MARS (ONSDAG).</b></br>
Efter det går det endast att betala i entrén till KBit (kontant eller swish. EJ Kort)</br>(200kr för datorplats / 50kr för super smash)</br> </br>
        Betalningsinformation kommer att skickas ut till den angivna mailadressen när vi mottagit er anmälan.</br>Anmälan är inte giltig innan betalning har inkommit till oss.
      </div>
		<div class="myform">
			<form method="post" action=" <?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="form-group">

			<div class="form-group row ">
				<label class="form-control-label col-sm-3" for="fnamn">Förnamn:</label>
				<div class="col-sm-9">
						<input type="text" class="form-control" id="fnamn" name="fnamn" value="">
					</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3" for="enamn"> Efternamn: </label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="enamn" name="enamn">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3" for="nick">Smeknamn:</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="nick" name="nick">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3" for="pernum" >Personnummer:</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="pernum" name="pernum" placeholder="ÅÅMMDDXXXX">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3" for="mail" >Mailadress: </label>
				<div class="col-sm-9">
					<input type="text" name="mail" class="form-control" id="mail">
				</div>
			</div>
			<div class="form-check row">
				<label class="form-check-label">
					<input class="form-check-input" name="rules" id="rules" type="checkbox"> Jag godkänner KBits <a href="#rules-div" data-toggle="collapse" aria-expanded="false" aria-controls="rules-div"> regler</a>
							</label>
				</div>
				<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="t_type" id="regular" value="vanlig" checked>
							Vanlig biljett (Datorplats/BYOC) - 150:-
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="t_type" id="smash" value="smash">
							Endast super smash turnering deltagarbiljett - 50:-
						</label>
			</div>
			<div class="form-check row">
				<button class="btn btn-primary" type="submit" value="Submit">Skicka in</button>
			</div>
			<input type="hidden" name="formcheck" value="true">
		</form></div>
		<?php
	}
	///--------------------------------------------------------------------------///
	function reg_form_error($fnamn,$enamn,$nick,$pernum,$mail,$rules,$t_type){
	?>
	<h3 class = "center padd"> Anmälan till KBit 2017</h3><br>
	<div class ="center">
	<p center><b>OBS!! SISTA CHANSEN ATT BETALA BILJETTEN ONLINE ÄR 29e MARS (ONSDAG).</b></br>
Efter det går det endast att betala i entrén till KBit (kontant eller swish. EJ Kort)</br>(200kr för datorplats / 50kr för super smash)</br> </br>
		Betalningsinformation kommer att skickas ut till den angivna mailadressen när vi mottagit er anmälan.</br> Anmälan är inte giltig innan betalning har inkommit till oss.
	</div>

	<div class="myform">
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="form-group">
			<?php
		if(!strlen($fnamn)) {
					 ?>
					<div class="form-group row has-danger">
						<label class="form-control-label col-sm-3" for="fnamn">Förnamn:</label>
						<div class="col-sm-9"><input type="text" class="form-control " id="fnamn" name="fnamn" >
							<div class="form-control-feedback">Fyll i Förnamn</div>
						</div>
				</div><?php
			}
		else{
			?>
			<div class="form-group row">
				<label class="form-control-label col-sm-3" for="fnamn">Förnamn:</label>
				<div class="col-sm-9"> <input type="text" class="form-control" id="fnamn" name="fnamn" value="<?=$fnamn?>"> </div>
			</div>
			<?php
		}
		if(!strlen($enamn)) {
			?>
			<div class="form-group row has-danger">
					<label class="form-control-label col-sm-3" for="enamn">Efternamn:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="enamn" name="enamn">
						<div class="form-control-feedback">Fyll i Efternamn</div>
					</div>
			</div><?php
			}
		else{
			?><div class="form-group row">
				<label class="form-control-label col-sm-3" for="enamn">Efternamn:</label>
				<div class="col-sm-9"><input type="text" class="form-control" id="enamn" name="enamn" value="<?=$enamn?>"></div>
			</div><?php
		}
		if(!strlen($nick)) {
			?><div class="form-group row has-danger">
					<label class="form-control-label col-sm-3" for="nick">Smeknamn:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="nick" name="nick" >
						<div class="form-control-feedback">Fyll i Smeknamn</div>
					</div>
				</div><?php
			}
		else{
			?><div class="form-group row">
					<label class="form-control-label col-sm-3" for="nick">Smeknamn:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="nick" name="nick"  value="<?=$nick?>"></div>
				</div><?php
		}
		if(!is_numeric($pernum) OR strlen($pernum) < 10) {
			?><div class="form-group row has-danger">
					<label class="form-control-label col-sm-3" for="pernum">Personnummer:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="pernum" name="pernum" placeholder="ÅÅMMDDXXXX" value="<?=$pernum?>">
						<div class="form-control-feedback">Ogiltigt personnummer</div>
					</div>
				</div><?php
			}
		else{
			?><div class="form-group row">
				<label class="form-control-label col-sm-3" for="pernum">Personnummer:</label>
				<div class="col-sm-9"><input type="text" class="form-control" id="pernum" name="pernum" placeholder="ÅÅMMDDXXXX" value="<?=$pernum?>"></div>
			</div><?php
		}

		if(!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			?><div class="form-group row has-danger">
				<label class="form-control-label col-sm-3" for="mail">Mailadress:</label>
				<div class="col-sm-9"><input type="text" name="mail" class="form-control" id="mail" value="<?=$mail?>">
					<div class="form-control-feedback">Ogiltig Mailadress</div>
				</div>
			</div><?php
			}
		else{
			?><div class="form-group row">
				<label class="form-control-label col-sm-3" for="mail">Mailadress:</label>
				<div class="col-sm-9"><input type="text" name="mail" class="form-control" id="mail" value="<?=$mail?>"></div>
			</div><?php
		}
		if(isset($rules)){
			?><div class="form-check row">
				<label class="form-check-label">
					<input class="form-check-input" name="rules" id="rules" type="checkbox"> Jag godkänner KBits <a href="#rules"> regler</a>
							</label>
				</div><?php
		}
		else{
			?><div class="form-check row has-danger">
				<label class="form-check-label">
					<input class="form-check-input" name="rules" id="rules" type="checkbox"> Jag godkänner KBits <a href="#rules"> regler</a>
							</label></div><?php
		}
		?>
		<div class="form-check">
				<label class="form-check-label">
					<input class="form-check-input" type="radio" name="t_type" id="regular" value="vanlig" <?php if($t_type == vanlig) echo "checked";?>>
					Vanlig biljett (Datorplats/BYOC) - 150:-
				</label>
			</div>
			<div class="form-check">
				<label class="form-check-label">
					<input class="form-check-input" type="radio" name="t_type" id="smash" value="smash" <?php if($t_type == smash) echo "checked";?>>
					Endast super smash turnering deltagarbiljett - 50:-
				</label>
			</div>
		<div class="form-check">
				<button class="btn btn-primary " type="submit" value="Submit">Submit</button></div>
				<input type="hidden" name="formcheck" value="true"></form></div><?php
	}

?>
