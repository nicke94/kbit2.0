<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
	<link href="css/my.css" rel="stylesheet">
  </head>
<body>
  <div class="container">
  <div class ="navbar-outer">
  <nav class="navbar center navbar-dark bg-inverse">
      <!--<a class="navbar-brand" href="#">KBit 2017</a>-->
    <a class="navbar-brand" href="#">KBit 2017</a>
      <ul class="nav navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">Hem </a>
        </li>
    <li class="nav-item ">
          <a class="nav-link" href="./tickets.php">Biljetter</a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="./tournaments.php">Turneringar</a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="./schedule.php">Schema</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./admin.php">Admin</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./contact.php">Kontakta oss</a>
        </li>
      </ul>
    </nav>
  </div>
  <img src="./img/web_top.png" class="img-fluid" alt="Responsive image">

  <?php
	include 'func.php';
	include 'connector.php';
	session_start();
  if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form

      $myusername = $mysqli->real_escape_string($_POST['username']);
      $mypassword = $mysqli->real_escape_string($_POST['password']);

      $sql = "SELECT id FROM login WHERE user= '$myusername' and pass = '$mypassword'";
      $result = $mysqli->query($sql);
      $row =  $result->fetch_assoc();
      $active = $row['active'];

      $count = $result->num_rows;

      // If result matched $myusername and $mypassword, table row must be 1 row

      if($count == 1) {
         $_SESSION['chk'] = "inlogg";
         header("location:admin.php");
      }
      else {
         echo "Your Login Name or Password is invalid";
      }
   }

	#print_r ($rad);

  ?><form action = "" method = "post">
                  <label>UserName  :</label><input type = "text" name = "username" class = "box"/><br /><br />
                  <label>Password  :</label><input type = "password" name = "password" class = "box" /><br/><br />
                  <input type = "submit" value = " Submit "/><br />
               </form>
	</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
