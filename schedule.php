<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS-->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	 <link href="css/my.css" rel="stylesheet">
  </head>
<body>
  <div class="wrapper">
      <div class = "l-background>">
        <img src="./img/web_bg_Left.png" class="img-fluid hidden-sm-down" alt="Responsive image">
      </div>
  <div class="container">
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">KBit 2017</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="nav navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="./index.php">Hem </a>
        </li>
    <li class="nav-item ">
          <a class="nav-link" href="./tickets.php">Biljetter</a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="./tournaments.php">Turneringar</a>
        </li>
    <li class="nav-item active">
          <a class="nav-link" href="./schedule.php">Schema <span class="sr-only">(current)</span></a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="./contact.php">Kontakta oss</a>
        </li>
      </ul>
      </div>
    </nav>
  <img src="./img/web_top.png" class="img-fluid" alt="Responsive image">
  <div class="starter-template">

        <h3>Fredag</h3>
        <table class="schedule table table-responsive">
          <thead>
            <tr>
              <th>Tid</th>
              <th>Händelse</th>
            </tr>
          </thead>
      		<tr><td class="time">15:00</td><td>LANet börjar byggas</td></tr>
      		<tr><td class="time">18:00</td><td>LANet öppnar officiellt. Det går att ta dit datorn tidigare men el och nätverk kan ej garanteras</td></tr>
      		<tr><td class="time">19:00</td><td>Deadline för att anmäla sig till Super Smash turneringen.</br></br>Tacobuffé går att köpa i Puben. Det går bra att äta framför datorn.
      									<br /><b>Observera att alkoholhaltig dryck EJ FÅR konsumeras utanför puben.</b></td></tr>
      		<tr><td class="time">20:40</td><td>Deadline för att anmäla sig till StarCraft 2 turneringen</td></tr>
		<tr><td class="time">21:00</td><td>Turneringarna börjar (Super Smash Bros Melee 1v1 & StarCraft 2)</td></tr>
      		<tr><td class="time">23:00</td><td>Super Smash Bros Melee 2v2 turnering börjar</td></tr>
      	</table>
        <h3>Lördag</h3>
        <table class="schedule table table-responsive">
          <thead>
            <tr>
              <th>Tid</th>
              <th>Händelse</th>
            </tr>
          </thead>
          <tr><td class="time">15:00</td><td>Turneringarna fortsätter</td></tr>

      	</table>
        <h3>Söndag</h3>
        <table class="schedule table table-responsive">
          <thead>
            <tr>
              <th>Tid</th>
              <th>Händelse</th>
            </tr>
          </thead>
          <tr><td class="time">18:00</td><td>Slut på LANet. dags att plocka ihop och ta sig hem =( </td></tr>
      	</table>




    </div>
    <img src="./img/web_bottom.png" class="img-fluid" alt="Responsive image">
    </div>
    <div class = "r-background>">
      <img src="./img/web_bg_Right.png" class="img-fluid hidden-sm-down" alt="Responsive image">
    </div>

	</div>

  <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
      <script src="js/bootstrap.min.js"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="js/ie10-viewport-bug-workaround.js"></script>
    </body>
  </html>
