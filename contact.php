<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="css/my.css" rel="stylesheet">
  </head>
<body>
  <div class="wrapper">
    <div class = "l-background>">
      <img src="./img/web_bg_Left.png" class="img-fluid hidden-sm-down" alt="Responsive image">
    </div>
  <div class="container">
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">KBit 2017</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav mr-auto">
        <li class="nav-item ">
          <a class="nav-link" href="./index.php">Hem </a>
        </li>
    <li class="nav-item ">
          <a class="nav-link" href="./tickets.php">Biljetter</a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="./tournaments.php">Turneringar</a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="./schedule.php">Schema</a>
        </li>
    <li class="nav-item active">
          <a class="nav-link" href="#">Kontakta oss</a>
        </li>
      </ul>
      </div>
    </nav>
  <img src="./img/web_top.png" class="img-fluid" alt="Responsive image">
    <div class="starter-template">
      <iframe class="gmap" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FkbitLAN&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

  	<h3>KBit kontakt</h3>
  	<p>
  		Har du frågor som inte besvarats av hemsidan?<br />Hör gärna av dig till oss på <a href="mailto:support@mdfnet.se">support@mdfnet.se</a>
      eller på vår Facebooksida.
    </p>

  	<h3>Besöksaddress</h3>

  	<p>Detta är besöksaddressen till arrangören</p>

  	<p>
  		<b>Mälardalens Datorförening <i>(MDF)</i><br />
  		Gustavsborgsgatan 6<br />
  		722 18 VÄSTERÅS</b>
  	</p>

  	<p>
  		Övre våningen, rakt in och sedan till höger.<br />
  		Öppet Måndag - fredag 13:00-17:00
  	</p>

  	<h3>MDF</h3>
  	<p>
  		MDF är en ideell förening som drivs av studenter för studenter.<br />
  		Det är en plats att umgås med likasinnade studenter med intresse för datorer, spel och nätverk. Det är dessa studenter som nu arrangerar detta LAN.
  	</p>

  	<p>
  		MDF levererar även billigt internet till studenter inkopplade i stadsnätet<br />
  		Mer info hittar du på våran hemsida:<br />
  		<a href="http://www.mdfnet.se">www.MDFnet.se</a>
  	</p>





  </div>
  <img src="./img/web_bottom.png" class="img-fluid" alt="Responsive image">
	</div>
  <div class = "r-background>">
    <img src="./img/web_bg_Right.png" class="img-fluid hidden-sm-down" alt="Responsive image">
  </div>

</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
