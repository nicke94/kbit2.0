<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

  <link href="css/my.css" rel="stylesheet">
  </head>
<body>

  <div class="wrapper">
    <div class = "l-background>">
      <img src="./img/web_bg_Left.png" class="img-fluid hidden-sm-down" alt="Responsive image">
    </div>

    <div class="container">

      <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand" href="#">KBit 2017</a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav mr-auto">
              <li class="nav-item ">
                <a class="nav-link" href="./.">Hem </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="./tickets.php">Biljetter</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Turneringar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="./schedule.php">Schema</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="./contact.php">Kontakta oss</a>
              </li>
            </ul>
          </div>
        </nav>


      <img src="./img/web_top.png" class="img-fluid" alt="Responsive image">

        <div class="starter-template">
        <h2>Turneringar </h2>

        <p>Nedan finns infomation om de turneringar som hålls under KBit</p>
        <!-- Nav tabs -->
<ul class="nav nav-tabs navbar-inverse" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#smash" role="tab">Super Smash</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#sc2" role="tab">Starcraft 2</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="smash" role="tabpanel">
    <h3>Turneringar i Super Smash Bros. Melee</h3>
    <img src="./img/SuperSmashMelee.png" class="img-fluid gmap" alt="Responsive image"> </img>
<p>
Alla som betalat KBits inträdesavgift kan vara med i turneringarna.</br>
Vill man bara deltaga på Melee-turnering och inte behöver datorplats så kostar det 50 kr.</br>
Ta gärna med egen kontroll då vi inte kan garantera utlåning.</br>

<h4>Singles (1v1)</h4>

Anmälningar ska vara inne muntligt eller genom email eller meddelande till Kbits Facebook senast kl 19.</br>
Pools börjar kl 21 på fredagen.</br>
Brackets börjar kl 15 på lördagen och fortsätter tills turneringen är klar.</br>
Formatet är pools och sedan double elimination bracket.</br>
Storlekar på pools och brackets beror på deltagarantal och annonseras innan turneringen börjar.</br></br>

Länk till regler (kommer snart)</br>
Länk till pools och brackets (kommer senare)</br></br>

<h4>Doubles (2v2)</h4>

Turneringen i doubles har lägre prioritet än den i singles och kan därför ske på mer obekväma tider.</br>
Doubles beräknas kunna börja ca 23 på fredagen, med en kort paus mellan singles och doubles.</br>
Eftersom det inte är helt klart när doubles kan börja så finns det ingen hård deadline till att anmäla sig, men gör det så fort som möjligt.</br>
Upplägg kommer bestämmas utefter deltagarantal och annonseras innan doubles-turneringen börjar.</br>
Om doubles inte hinner spelas klart på fredagen samt en stund in på lördagsnatten så kommer det fortsätta efter singles har spelats klart på lördagen.</br></br>

Länk till regler (kommer snart)</br>
Länk till pools och brackets (kommer senare)</br></br>

Vid frågor:</br>
Kontakta Kbit på <a href="https://www.facebook.com/kbitLAN/"> Facebook </a></br>
eller</br>
kontakta turneringsansvarig Jonas Larsson genom email: <a href="mailto:Jukk.88@gmail.com">Jukk.88@gmail.com</a> </br></p>

  </div>
  <div class="tab-pane" id="sc2" role="tabpanel">
    <h3>Turnering i StarCraft 2</h3>
    <img src="./img/StarCraft2.png" class="img-fluid gmap" alt="Responsive image"> </img>

<p>Alla som betalat KBits inträdesavgift kan vara med i turneringen, och antas därför ha egen dator med sig att spela på.</br>
Turneringsformatet är BO3 i varje match samt double elimination (Lower och Upper brackets)</br></br>

Det går bra att anmäla sig till turneringen på plats</br></br>

Mer info kommer senare</br>

Vid frågor:</br>
Kontakta KBit på <a href="https://www.facebook.com/kbitLAN/"> Facebook </a></p>

  </div>

</div>



        </div>
  	</div>
    <div class = "r-background>">
      <img src="./img/web_bg_Right.png" class="img-fluid hidden-sm-down" alt="Responsive image">
    </div>

</div>
<img src="./img/web_bottom.png" class="img-fluid" alt="Responsive image">
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
