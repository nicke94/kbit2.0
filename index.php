<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link href="css/my.css" rel="stylesheet">
  </head>
  <body>
    <div class="wrapper">
      <div class = "l-background>">
        <img src="./img/web_bg_Left.png" class="img-fluid hidden-sm-down" alt="Responsive image">
      </div>
  <div class="container">
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="#">KBit 2017</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="nav navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="#">Hem </a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="./tickets.php">Biljetter</a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="./tournaments.php">Turneringar</a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="./schedule.php">Schema</a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="./contact.php">Kontakta oss</a>
        </li>
      </ul>
      </div>
    </nav>
	<img src="./img/web_top.png" class="img-fluid" alt="Responsive image">
  <div class="starter-template">
    <?php
      include 'connector.php';
      $sql = "SELECT * from backend";
      $resultat = $mysqli->query($sql);
      $startdate = $resultat->fetch_assoc();
      #echo "<h1>Årets KBit öppnar den " . $startdate['date'] ." " . $startdate['month']."!</h1>";
     ?>

		<h1>KBit 2017 är nu över. Men vi ses igen nästa år!</h1>
	   </br>	

		<h3>Vad är KBit?</h3>
		<p>
      KBit är ett LAN som arrangeras av Mälardalens Datorförening (MDF) varje vårtermin. Majoriteten av deltagarna brukar vara MDH studenter, men alla som fyllt 15 år eller har med sig en ansvarstagande myndig får vara med.
		Deltagare tar med sin egna dator för att spela spel och delta i turneringar.
		</p><iframe class="gmap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2017.8694736465484!2d16.54078261639531!3d59.61853438176099!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x465e61486cd863a5%3A0x7df6b496a02b7de9!2sGustavsborgsgatan+6%2C+722+18+V%C3%A4ster%C3%A5s!5e0!3m2!1ssv!2sse!4v1488146210269" width="300" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>

    <h3>Var är KBit?</h3>
		<p>
			Mälardalens Studentkårs kårhus i Västerås. Kåren ligger precis bredvid Mälardalens Högskola.<br />
			Addressen till KBit är: <b>Gustavsborgsgatan 6, 722 18 Västerås</b>
      </p>



		<h3>Hur är KBit?</h3>
		<p>
      Superkul! Ta med kompisarna och datorn och gibba loss en helg i "vintermörkret".
		Det brukar arrangeras turneringar i spel så som Dota 2, StarCraft 2 och Super Smash bros Melee m.fl. Se mer under fliken Turneringar.<br /><br />

		Under fredagkvällen serverar Kåren enligt tradition en Tacobuffé i puben där man även kan köpa dricka (Obs! ej alkoholförtäring i LAN-lokalen).<br /></p>
    <p>
		<b>BYOC - Bring Your Own Computer</b></br>

    Innebär som det låter. Ta med din dator med alla tillbehör. Vi står för bordplats, stol, el, internet. Det brukar även gå att ta med egen stol för extra komfort, men om det blir väldigt trångt så förbehåller vi oss rätten att begränsa användandet av egen stol till bakre bordsraden.
		</p>
    <p>
		<b>VR - HTC Vive</b></br>
    Under hela fredag kväll går det att prova på VR med en <a href="https://www.vive.com" target="_blank">HTC Vive</a> !
Missa inte chansen att uppleva en virtuell värld som även känner av hur du förflyttar dig i rummet. Endast tillgängligt under fredag kväll.
  </p>

		<h3>Vad kostar KBit?</h3>
		<p>
			150 kr om du <a href="tickets.php">förköper</a> biljett. BYOC (Datorplats inkl. stol/el/internet)<br />
			200 kr om du betalar i entrén. BYOC (Datorplats inkl. stol/el/internet)<br />
      50 kr för att endast delta i Super Smash Melee turneringen. (datorplats ingår ej) <br/><br />

      Det finns datorplatser för upp till ca 80 personer. Mer info hittar du under fliken <a href="tickets.php">Biljetter</a>.
		</p>
      </div>
	       <img src="./img/web_bottom.png" class="img-fluid" alt="Responsive image">
    </div><!-- /.container -->
    <div class = "r-background>">
      <img src="./img/web_bg_Right.png" class="img-fluid hidden-sm-down" alt="Responsive image">
    </div>
</div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <script src="js/ie10-viewport-bug-workaround.js"></script>-->
  </body>

</html>
