<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

  <link href="css/my.css" rel="stylesheet">
  </head>
<body>

  <div class="wrapper">
    <div class = "l-background>">
      <img src="./img/web_bg_Left.png" class="img-fluid hidden-sm-down" alt="Responsive image">
    </div>

    <div class="container">

      <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand" href="#">KBit 2017</a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav mr-auto">
              <li class="nav-item ">
                <a class="nav-link" href="./.">Hem </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="./tickets.php">Biljetter</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Turneringar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="./schedule.php">Schema</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="./contact.php">Kontakta oss</a>
              </li>
            </ul>
          </div>
        </nav>


      <img src="./img/web_top.png" class="img-fluid" alt="Responsive image">

        <div class="starter-template">
        <h2>Turneringar </h2>

        <p>Nedan finns infomation om de turneringar som hålls under KBit</p>
        <!-- Nav tabs -->
<ul class="nav nav-tabs navbar-inverse" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#smash" role="tab">Super Smash</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#sc2" role="tab">Starcraft 2</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="smash" role="tabpanel">
    <h3>Turneringar i Super Smash Bros. Melee</h3>
    <img src="./img/SuperSmashMelee.png" class="img-fluid gmap" alt="Responsive image"> </img>
<p>
Alla som betalat KBits inträdesavgift kan vara med i turneringarna.</br>
Vill man bara deltaga på Melee-turnering och inte behöver datorplats så kostar det 50 kr.</br>
Ta gärna med egen kontroll då vi inte kan garantera utlåning.</br>
<h4>Anmälan </h4>
Anmälan till turneringen måste vara inne senast kl 19:00 på fredagen (31 mars). Det går bra att anmäla sig redan nu i <a href="https://goo.gl/forms/xkF4WA2S5SIykwGy2"> detta formulär</a>, men du kan även ta kontakt med turneringsansvarig Jonas Larsson på plats.</br></br>


<h4>Singles (1v1)</h4>


Pools börjar kl 21 på fredagen.</br></br>
Brackets börjar kl 15 på lördagen och fortsätter tills turneringen är klar.</br>
Formatet är pools och sedan double elimination bracket.</br>
Storlekar på pools och brackets beror på deltagarantal och annonseras innan turneringen börjar.</br></br>

  <a href="https://docs.google.com/document/d/1TvWCT7VXCCG-ExD5E8E0aICiaoXWl0MJuupX0EnOLdc" target="_blank">Länk</a> till regler</br>
Länk till pools och brackets (kommer senare)</br></br>

<h4>Doubles (2v2)</h4>

Turneringen i doubles har lägre prioritet än den i singles och kan därför ske på mer obekväma tider.</br>
Doubles beräknas kunna börja ca 23 på fredagen, med en kort paus mellan singles och doubles.</br>
Eftersom det inte är helt klart när doubles kan börja så finns det ingen hård deadline till att anmäla sig, men gör det så fort som möjligt.</br>
Upplägg kommer bestämmas utefter deltagarantal och annonseras innan doubles-turneringen börjar.</br>
Om doubles inte hinner spelas klart på fredagen samt en stund in på lördagsnatten så kommer det fortsätta efter singles har spelats klart på lördagen.</br></br>

<a href="https://docs.google.com/document/d/1TvWCT7VXCCG-ExD5E8E0aICiaoXWl0MJuupX0EnOLdc/edit#bookmark=id.ramywkmh2m7e" target="_blank">Länk</a> till regler</br>
Länk till pools och brackets (kommer senare)</br></br>

Vid frågor:</br>
Kontakta Kbit på <a href="https://www.facebook.com/kbitLAN/"> Facebook </a></br>
eller</br>
kontakta turneringsansvarig Jonas Larsson genom email: <a href="mailto:Jukk.88@gmail.com">Jukk.88@gmail.com</a> </br></p>

  </div>
  <div class="tab-pane" id="sc2" role="tabpanel">
    <h3>Turnering i StarCraft 2</h3>
    <img src="./img/StarCraft2.png" class="img-fluid gmap" alt="Responsive image"> </img>

<p>
  <h4>Anmälan</h4>
Alla som betalat KBits inträdesavgift kan vara med i turneringen, och antas därför ha egen dator med sig att spela på.</p><p>

Anmälan till turneringen måste vara inne senast kl 20:00 på fredagen (31 mars). Det går bra att anmäla sig redan nu i <a href="https://goo.gl/forms/ZC1mHsbfe2invIQu1" target="_blank"> detta formulär</a>, men du kan även ta kontakt med admin, Christian eller Murat på plats.</p><p>

<h4>Turneringsformen</h4>
Turneringsformen är Double Elimination och varje omgång är som följer:</br>
<b>Winners bracket:</b></br> BO3, utom semi-final som är BO5, och slutfinal som är först en BO7,</br>
<b>Losers bracket:</b></br> BO3, utom final som är BO5 (där vinnaren går till slutfinalen)</p><p>

Turneringen börjar kl 21:00 på fredagen och fortsätter kl 15 på lördagen tills turneringen är klar, hur sent matcherna spelas under fredagen beror på deltagarantal.</p><p>

<h4>Kartor</h4>
Det finns en map-pool där det går till så att första matchen i varje omgång måste vara enligt listan på challonge, Därefter sker en Mapveeto där spelarna väljer bort  Karor tills det återstår 3 st om det är en BO3. båda väljer därefter en varsin map de vill spela på och om det skulle stå lika (1-1 i matcher) så väljs automatiskt den kartan som valdes sist.

Första pris är en mus från SteelSeries.</p><p>

Länk till regler och brackets <a href="http://challonge.com/sv/kbitstarcraft" target="_blank"> http://challonge.com/sv/kbitstarcraft</a></p><p>

Vid frågor:</br>
Kontakta KBit på <a href="https://www.facebook.com/kbitLAN/"> Facebook </a></br>
eller</br>
kontakta turneringsansvarig Christan Hurtig genom email: <a href="mailto:christianhurtig@hotmail.com">christianhurtig@hotmail.com</a></br></p>


  </div>

</div>



        </div>
        <img src="./img/web_bottom.png" class="img-fluid" alt="Responsive image">
  	</div>
    <div class = "r-background>">
      <img src="./img/web_bg_Right.png" class="img-fluid hidden-sm-down" alt="Responsive image">
    </div>

</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
