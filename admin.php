<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon">
    <title>KBit 2017</title>
    <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
	<link href="css/my.css" rel="stylesheet">
  </head>
  <body>
    <div class="container gray">
      <div class ="navbar-outer">
      <nav class="navbar center navbar-dark bg-inverse">
           <!--<a class="navbar-brand" href="#">KBit 2017</a>-->
        <a class="navbar-brand" href="#">KBit 2017</a>
           <ul class="nav navbar-nav">
             <li class="nav-item ">
               <a class="nav-link" href="./index.php">Hem </a>
             </li>
        <li class="nav-item">
               <a class="nav-link" href="./tickets.php">Biljetter</a>
             </li>
        <li class="nav-item">
               <a class="nav-link" href="./tournaments.php">Turneringar</a>
             </li>
        <li class="nav-item">
               <a class="nav-link" href="./schedule.php">Schema</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="./contact.php">Kontakta oss</a>
             </li>
           </ul>
         </nav>
      </div>
    </div>

<?php
	include 'connector.php';
	include 'func.php';
  chk();
  if ($_POST['log'] == "utlogg"){
		session_destroy();
		header('location:login.php?med=utloggad');
	}
	if(isset($_POST['formcheck'])){
		//echo "debug</br>";
		$sql = "UPDATE registered SET payed = null";
		$resultat = $mysqli->query($sql);

		if(!empty($_POST['paystatus'])){
			foreach($_POST['paystatus'] as $selected){
				$sql = "UPDATE registered SET payed = 1 WHERE id=".$selected;
				$resultat = $mysqli->query($sql);
				//echo $sql."</br>";
			}
		}
	}
	if(isset($_POST['formcheck-backend'])){
		//echo "debug</br>";
		$sql = "UPDATE backend SET enabled = \"".$_POST['enabled'] ."\", date = \"".$_POST['dag'] ."\",year = \"".$_POST['year'] ."\",month = \"".$_POST['month']."\"";
		//echo $sql;
		$resultat = $mysqli->query($sql);

	}

	$sql = "SELECT * from backend";
	$resultat = $mysqli->query($sql);

	$rad = $resultat->fetch_assoc();
	//print_r ($rad);
	echo "<div class=\"container startdate \">";
	//echo "<div class=\"f-left\">";
	echo "<form method=\"post\" action=\"admin.php\" class=\"\">";
  echo "<legend>Kbit Startdatum </legend>";
	echo "<div class=\"form-group row\">
    <label class=\"col-sm-1\" for =\"dag\">Dag:</label>
      <div class=\"col-sm-1\">
      <input type=\"text\" class=\"form-control\" id=\"dag\" name=\"dag\" value=\"".$rad['date']."\">
      </div>
  </div>";
	echo "<div class=\"form-group row\">
  <label class=\"col-sm-1\" for =\"month\">Månad:</label>
  <div class=\"col-sm-1\">
    <input type=\"text\" class=\"form-control\" id=\"month\" name=\"month\" value=\"".$rad['month']."\">
  </div>
  </div>";
	echo "<div class=\"form-group row\">
  <label class=\"col-sm-1\" for =\"year\">År:</label>
    <div class=\"col-sm-1\">
  <input type=\"text\" class=\"form-control\" id=\"year\" name=\"year\" value=\"".$rad['year']."\">
  </div>
  </div>";
	echo "<div class=\"form-check\"><label class=\"form-check-label\"><input type=\"checkbox\" class=\"form-check-input\" name=enabled value=".$rad['id'];
	if ($rad['enabled']){
		echo " checked ";
	}

	echo "> Anmälan öppen</label></div><input type=\"hidden\" name=\"formcheck-backend\" value=\"true\"><button class=\"btn btn-primary\" type=\"submit\" value=\"Submit\">Submit</button></form></div></div>";




	$sql = "SELECT * from registered";
	$resultat = $mysqli->query($sql);
	$antal = $resultat->num_rows;
	//echo $sql;
	//echo $antal;

	echo "<div class=\"container gray padd\">";
  echo "<h3> Anmälningar </h3>";

	if ($antal == 0) {
			echo "<div class=\"info\"<h3>Inga registrerade</h3></div></form><br>";
	}
	else {
		echo "<div><form method=\"post\" action=\"admin.php\"><table class=\"table table-striped\"><thead><tr><th>Tid</th><th>Förnamn</th><th>Efternamn</th><th>Nick</th><th>Emailadress</th><th>personnummer-id</th><th>Biljetttyp</th><th>Betalat</th></tr></thead><tbody>";
		while ($rad = $resultat->fetch_assoc()) {
			if ($rad['payed'] != null){
				$payed = "<input type=\"checkbox\" name=paystatus[] value=".$rad['id']." checked >";
        echo '<tr class="bggreen">';
			}
			else {
				$payed = "<input type=\"checkbox\" name=paystatus[] value=".$rad['id'].">";
        echo '<tr>';
			}

			echo "<td>".$rad['time'] ."</td><td>".$rad['fname']."</td><td>".$rad['ename']."</td><td>".$rad['nick']."</td><td>".$rad['email']."</td><td>".$rad['pernum']."-".$rad['id']."</td><td>".$rad['type']."</td><td>".$payed ."</td></tr></div>";

		}
		echo "</tbody></table><button class=\"btn btn-primary \" type=\"submit\" value=\"Submit\">Submit</button></div></fieldset><input type=\"hidden\" name=\"formcheck\" value=\"true\"></form>";
	}
	//echo "</div>";
	//tbk();
	//echo "<a href=\"mail.php\"><button class=\"button-b right\">mail test</button></a>";

	$resultat->free();
	$mysqli->close();
	echo "</div>";
	?>
  <form method="post" action="">
		<input type="hidden" name="log" value="utlogg"></br>
        <button type="submit">Logga ut </button>
    </form>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
